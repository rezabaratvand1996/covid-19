import React , {useEffect , useState} from 'react';
import axios from 'axios'
import 'bootstrap/dist/css/bootstrap.min.css'
import Card from 'react-bootstrap/Card'

function Cases() {
    
    const [latest ,setLatest] = useState([])

    useEffect(()=>{
        axios.get("https://corona.lmao.ninja/all")
          .then(res =>{
            setLatest(res.data);
          })
          .catch(err => {
            console.log(err)
          })
      }, []);
    
      const date = new Date(parseInt(latest.updated))
      const lastUpdated = date.toString();
    
  return (
    <div>
        <Card bg="warning" text={"white"} className="text-center" style={{margin:"10px"}} >
            <Card.Body>
                <Card.Title>مبتلا شده ها</Card.Title>
                <Card.Text>{latest.cases}</Card.Text>
            </Card.Body>
            <Card.Footer>
                <small >Last updated {lastUpdated}</small>
            </Card.Footer>
        </Card>
    </div>
  );
}

export default Cases;
