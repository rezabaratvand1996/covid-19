import React , {useState ,useEffect} from 'react';
import Card from 'react-bootstrap/Card'
import axios from 'axios'


function Countries(props) {
    const [results , setResults] = useState([])

    useEffect(()=>{
        axios.get("https://corona.lmao.ninja/countries")
          .then(res =>{
            setResults(res.data)
           
          })
          .catch(err => {
            console.log(err)
          })
      }, []);
    return (
        <div>
        {results.map(data => {
            return (
                <Card bg="light" className="text-center" style={{margin:"10px"}} >
                <Card.Body>
                    
                    <Card.Title>{data.country}</Card.Title>
                    <Card.Text>مبتلا شده ها {data.cases} </Card.Text>
                    <Card.Text>جان باختگان {data.deaths} </Card.Text>
                    <Card.Text>بهبود یافتگان {data.recovered} </Card.Text>
                    <Card.Text>مبتلایان امروز {data.todayCases} </Card.Text>
                    <Card.Text>جان باختگان امروز {data.todayDeaths} </Card.Text>
                    <Card.Text>باقی مانده ها {data.active} </Card.Text>
                    <Card.Text> بحرانی ها {data.critical} </Card.Text>
                </Card.Body>
            </Card>
            )
        })}
        </div>
    );
}

export default Countries;
