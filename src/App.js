import React , {useEffect , useState} from 'react';
import axios from 'axios'
import 'bootstrap/dist/css/bootstrap.min.css'
import CardDeck from 'react-bootstrap/CardDeck'
import './App.css'
import SearchField from './components/SearchField/SearchField'
import Countries from './components/countries/Countries'
import Cases from './components/Cases/Cases'
import Deaths from './components/Deaths/Deaths'
import Recovered from './components/Recovered/Recovered'
function App() {

  const [latest ,setLatest] = useState([])

  useEffect(()=>{
    axios.get("https://corona.lmao.ninja/all")
      .then(res =>{
        setLatest(res.data);
      })
      .catch(err => {
        console.log(err)
      })
  }, []);
  return (
    <div className="App">
      <CardDeck>
          <Cases/>
          <Deaths/>
          <Recovered/>
      </CardDeck>
          <SearchField/>
          <Countries/>
    </div>
  );
}

export default App;
